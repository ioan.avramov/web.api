﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DA.DataModels.Entities
{
   public class Comment : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Text { get; set; }
        public int Likes { get; set; }
        public double Dislikes { get; set; }
        public int OwnerId { get; set; }

        public User Owner { get; set; }

        public int VideoId { get; set; }

        public Video Video_ { get; set; }
    }
}
