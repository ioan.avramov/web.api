﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DA.DataModels.Entities
{
   public class User : BaseEntity
    {
        [MaxLength(50)]
        [Required]
        public string Username { get; set; }
        [MaxLength(50)]
        [Required]
        public string First_Name { get; set; }
        [MaxLength(50)]
        [Required]
        public string Last_Name { get; set; }

        public double Age { get; set; }

        [MaxLength(50)]
        [Required]
        public string Password { get; set; }

        public bool Gender { get; set; }

        public virtual ICollection<Video> VideosAdded { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
