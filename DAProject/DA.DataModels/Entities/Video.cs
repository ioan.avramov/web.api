﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DA.DataModels.Entities
{
   public class Video : BaseEntity
    {
        [MaxLength(30)]
        [Required]
        public string Title { get; set; }

        [MaxLength(50)]
        [Required]
        public string Link { get; set; }
        public bool Over18 { get; set; }
        public int Likes { get; set; }
        public int OwnerId { get; set; }
        public User Owner { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
