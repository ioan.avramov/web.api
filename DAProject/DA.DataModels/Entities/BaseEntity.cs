﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA.DataModels.Entities
{
   public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime DateAdded { get; set; }

    }
}
