﻿using DA.DataModels.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace DA.DataWork
{
    public class DAProjectDBContext : DbContext
    {
        public DAProjectDBContext() : base()
        { }

        public virtual DbSet<User> User { get; set; }

        public virtual DbSet<Video> Video { get; set; }

        public virtual DbSet<Comment> Comment { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost;Database=VideoHubDb;User Id=ioandb;Password=ioandb;");

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(n => n.Username)
                .HasMaxLength(30)
                .IsRequired();

            modelBuilder.Entity<User>()
               .Property(n => n.Password)
               .HasMaxLength(30)
               .IsRequired();

            modelBuilder.Entity<Video>()
                .HasOne(v => v.Owner)
                .WithMany(m => m.VideosAdded)
                .HasForeignKey(u => u.OwnerId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Comment>()
                .HasOne(v => v.Video_)
                .WithMany(u => u.Comments)
                .HasForeignKey(f => f.VideoId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Comment>()
                .HasOne(u => u.Owner)
                .WithMany(c => c.Comments)
                .HasForeignKey(u => u.OwnerId)
                .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
        }
    }
}
