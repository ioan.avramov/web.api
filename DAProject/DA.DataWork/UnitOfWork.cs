﻿using DA.DataModels.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.DataWork
{
    public class UnitOfWork : IDisposable
    {
        private readonly DAProjectDBContext dbContext;
        private BaseRepository<User> userRepository;
        private BaseRepository<Video> videoRepository;
        private BaseRepository<Comment> commentRepository;
        private bool disposed = false;

        public UnitOfWork()
        {
            this.dbContext = new DAProjectDBContext();
        }

        public BaseRepository<User> UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new BaseRepository<User>(dbContext);
                }

                return userRepository;
            }
        }

        public BaseRepository<Video> VideoRepository
        {
            get
            {
                if (this.videoRepository == null)
                {
                    this.videoRepository = new BaseRepository<Video>(dbContext);
                }

                return videoRepository;
            }
        }

        public BaseRepository<Comment> CommentRepository
        {
            get
            {
                if (this.commentRepository == null)
                {
                    this.commentRepository = new BaseRepository<Comment>(dbContext);
                }

                return commentRepository;
            }
        }

        public bool Save()
        {
            try
            {
                dbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }

                disposed = true;
            }
        }
    }
}
