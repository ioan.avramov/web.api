﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using DA.Website.Models.UserModel;
using DA.Website.Models.VideoModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DA.Website.Controllers
{
    public class VideoController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private const string AUTHORIZATION_HEADER_NAME = "Authorization";
        private readonly Uri videoUri = new Uri("http://localhost:57868/api/video");
        private readonly Uri loginUri = new Uri("http://localhost:57868/api/login");

        // GET: Video ----------------- Done
        [HttpGet]
        public async Task<ActionResult> Index(string title)
        {
            using (var client = new HttpClient())
            {
                var token =  GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login","Home");
               
                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.GetAsync($"{videoUri}?title={title}");

                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");
                
                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<IEnumerable<VideoVM>>(jsonResponse);

                return View(responseData);
            }
        }

        // GET: Video/Details/video1 ----------------- Done
        public async Task<ActionResult> Details(int id)
        {
            using (var client = new HttpClient())
            {

                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.GetAsync($"{videoUri}/{id}");
               
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");
               
                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<VideoVM>(jsonResponse);

                return View(responseData);
            }
        }

        // GET: Video/Create ----------------- Done
        public ActionResult Create()
        {
            return View();
        }

        // POST: Video/Create ----------------- Done
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(VideoCreateVM model)
        {
            if (ModelState.IsValid)
            {
                using (HttpClient client = new HttpClient())
                {
                    var token = GetAccessToken();
                    if (string.IsNullOrEmpty(token))
                        return RedirectToAction("Login", "Home");

                    client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                    //That doesn't work unless you log in everytime
                    UserVM curr = new UserVM();
                    curr.Id = LoggedUser.Id;
                    curr.Username = LoggedUser.Username;
                    //----------------------------------------------

                    var serializedContent = JsonConvert.SerializeObject(new { Title = model.Title, Link = model.Link, Over18 = model.Over18, OwnerId = curr.Id});
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(videoUri, stringContent);
                    if (Auth(response.StatusCode))
                        return RedirectToAction("Login", "Home");

                    if (!response.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError("VideoError", "Such video already exists!");
                        return View(model);
                    }
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Video");
        }

        // GET: Video/Edit/5 ----------------- Done
        public async Task<ActionResult> Edit(int id)
        {
            using (var client = new HttpClient())
            {

                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.GetAsync($"{videoUri}/{id}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<VideoCreateVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Video/Edit/5 ----------------- Done
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(VideoCreateVM model)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {

                    var token = GetAccessToken();
                    if (string.IsNullOrEmpty(token))
                        return RedirectToAction("Login", "Home");

                    client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                    var serializedContent = JsonConvert.SerializeObject(new { Title = model.Title, Link = model.Link, Over18 = model.Over18});
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{videoUri}/{model.Id}", stringContent);
                    if (Auth(response.StatusCode))
                        return RedirectToAction("Login", "Home");

                    if (!response.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError("VideoError", "Such video already exists!");
                        return View(model);
                    }
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Video");
        }

        // ----------------- Done
        // check if the video delete will still be processed if HttpGet is removed
        [HttpGet, ActionName("Delete")]
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.DeleteAsync($"{videoUri}/{id}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }
            }

            return RedirectToAction("Index", "Video");
        }
        
        private string GetAccessToken()
        {
            var myCookie = Request.Cookies["accessToken"];

            if (!string.IsNullOrEmpty(myCookie))
            {
                return myCookie;
            }
            return null;
        }
    
        private bool Auth(HttpStatusCode status_code)
        {
            if (status_code == HttpStatusCode.Unauthorized)
            {
                return true;  
            }
            return false;
        }
    }
}