﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DA.Website.Models.HomeModels;
using DA.Website.Models.UserModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DA.Website.Controllers
{
    public class UserController : Controller
    {

        private const string JSON_MEDIA_TYPE = "application/json";
        private const string AUTHORIZATION_HEADER_NAME = "Authorization";
        private readonly Uri userUri = new Uri("http://localhost:57868/api/user");

        // GET: User --------------- Done
        public async Task<ActionResult> Index(string username)
        {
            using (HttpClient client = new HttpClient())
            {
                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.GetAsync($"{userUri}?username={username}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<IEnumerable<UserVM>>(jsonResponse);

                return View(responseData);
            }
        }

        // GET: User/Details/ --------------- Done
        public async Task<ActionResult> Details(string username)
        {
            using (HttpClient client = new HttpClient())
            {
                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);
               
                HttpResponseMessage response = await client.GetAsync($"{userUri}?username={username}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<IEnumerable<UserVM>>(jsonResponse).FirstOrDefault();

                return View(responseData);
            }
        }

        // Get: User/Create --------------- Done
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create --------------- Done
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterVM registerModel)
        {
            if (ModelState.IsValid)
            {
                using (HttpClient client = new HttpClient())
                {
                    var serializedContent = JsonConvert.SerializeObject(new { Username = registerModel.Username, Password = registerModel.Password, Age = registerModel.Age,
                                                                              Gender = registerModel.Gender, First_Name = registerModel.First_Name, Last_Name = registerModel.Last_Name});
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(userUri, stringContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError("RegisterError", "This user already exists!");
                        return View(registerModel);
                    }
                }
                return RedirectToAction("Login", "Home");
            }
            return View(registerModel);
        }

        // GET: User/Edit/5 --------------- Done
        public async Task<ActionResult> Edit(RegisterVM model)
        {
            using (var client = new HttpClient())
            {
                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                var serializedContent = JsonConvert.SerializeObject(new { Username = model.Username });
                var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                HttpResponseMessage response = await client.PostAsync(userUri, stringContent);
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<RegisterVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: User/Edit/5 --------------- Done
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id,RegisterVM model)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {

                    var token = GetAccessToken();
                    if (string.IsNullOrEmpty(token))
                        return RedirectToAction("Login", "Home");

                    client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                    var serializedContent = JsonConvert.SerializeObject(new { Username = model.Username, Password = model.Password, Age = model.Age, 
                                                                              Gender = model.Gender, First_Name = model.First_Name, Last_Name = model.Last_Name});
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{userUri}/{model.Id}", stringContent);
                    if (Auth(response.StatusCode))
                        return RedirectToAction("Login", "Home");

                    if (!response.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError("UserUpdateError", "This username is alredy taken!");
                        return View(model);
                    }
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            LoggedUser.Username = model.Username;
            return RedirectToAction("Details", "User", new { username = model.Username });
        }

        // POST: User/Delete/5  --------------- Done
        [HttpGet, ActionName("Delete")]
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.DeleteAsync($"{userUri}/{id}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                return RedirectToAction("Logout", "Home");
            }
           
        }

        private string GetAccessToken()
        {
            var myCookie = Request.Cookies["accessToken"];

            if (!string.IsNullOrEmpty(myCookie))
            {
                return myCookie;
            }
            return null;
        }
        private bool Auth(HttpStatusCode status_code)
        {
            if (status_code == HttpStatusCode.Unauthorized)
            {
                return true;
            }
            return false;
        }
    }
}