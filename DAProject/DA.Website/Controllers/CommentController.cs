﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DA.Website.Models.CommentModels;
using DA.Website.Models.UserModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DA.Website.Controllers
{
    public class CommentController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private const string AUTHORIZATION_HEADER_NAME = "Authorization";
        private readonly Uri commentUri = new Uri("http://localhost:57868/api/comment");

        // GET:  --------- no need
        public ActionResult Index()
        {
            return View();
        }

        // GET:  ------------ no need
        public ActionResult Details(int id)
        {
            return View();
        }

        
        // GET: Comment/Create  ----------------- Done
        public ActionResult Create(int id)
        {
            CommentEditVM model = new CommentEditVM();
            model.VideoId = id;
            model.OwnerId = LoggedUser.Id;
            return View(model);
        }

       
        // POST: Comment/Create ----------------- Done
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CommentEditVM model)
        {
            if (ModelState.IsValid)
            {
                using (HttpClient client = new HttpClient())
                {
                    var token = GetAccessToken();
                    if (string.IsNullOrEmpty(token))
                        return RedirectToAction("Login", "Home");

                    client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                    var serializedContent = JsonConvert.SerializeObject(new { Text = model.Text, VideoId = model.VideoId, OwnerId = model.OwnerId });
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(commentUri, stringContent);
                    if (Auth(response.StatusCode))
                        return RedirectToAction("Login", "Home");

                    if (!response.IsSuccessStatusCode)
                    {
                        return View(model);
                    }
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Details", "Video", new { id = model.VideoId });
        }
       
        
       
        // GET: Comment/Edit/5 ----------------- Done
        public async Task<ActionResult> Edit(int id)
        {
            using (var client = new HttpClient())
            {

                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.GetAsync($"{commentUri}/{id}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<CommentEditVM>(jsonResponse);

                return View(responseData);
            }
        }

        
        
        // POST: Comment/Edit/5 ----------------- Done
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, CommentEditVM model)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {

                    var token = GetAccessToken();
                    if (string.IsNullOrEmpty(token))
                        return RedirectToAction("Login", "Home");

                    client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                    var serializedContent = JsonConvert.SerializeObject(new { Text = model.Text });
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{commentUri}/{id}", stringContent);
                    if (Auth(response.StatusCode))
                        return RedirectToAction("Login", "Home");

                    if (!response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Error", "Home");
                    }
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Details", "Video", new { id = model.VideoId });
        }


       
        // POST: Comment/Delete/5 ----------------- Done
        [HttpGet, ActionName("Delete")]
        public async Task<ActionResult> Delete(int id, int videoId)
        {
            using (HttpClient client = new HttpClient())
            {
                Debug.WriteLine(videoId);
                var token = GetAccessToken();
                if (string.IsNullOrEmpty(token))
                    return RedirectToAction("Login", "Home");

                client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, token);

                HttpResponseMessage response = await client.DeleteAsync($"{commentUri}/{id}");
                if (Auth(response.StatusCode))
                    return RedirectToAction("Login", "Home");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(HomeController.Error), "Home");
                }
            }

             return RedirectToAction("Details","Video", new { id = videoId });
        }

        private string GetAccessToken()
        {
            var myCookie = Request.Cookies["accessToken"];

            if (!string.IsNullOrEmpty(myCookie))
            {
                return myCookie;
            }
            return null;
        }

        private bool Auth(HttpStatusCode status_code)
        {
            if (status_code == HttpStatusCode.Unauthorized)
            {
                return true;
            }
            return false;
        }
    }
}