﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DA.Website.Models;
using Newtonsoft.Json;
using System.Text;
using System.Web;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using DA.Website.Models.UserModel;
using System.Net;

namespace DA.Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private const string JSON_MEDIA_TYPE = "application/json";
        private const string AUTHORIZATION_HEADER_NAME = "Authorization";
        private readonly Uri loginUri = new Uri("http://localhost:57868/api/login");
        private readonly Uri usersUri = new Uri("http://localhost:57868/api/user");
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        
        // ----------------- Done
        public IActionResult Index()
        {
            return View();
        }
        
        
        // ----------------- Done
        [HttpGet]
        public IActionResult LogIn()
        {
            return View();
        }

        // ----------------- Done
        [HttpPost]
        public async Task<IActionResult> Login(LogInModel model)
        {
            if (ModelState.IsValid)
            {
                //Debug.WriteLine(model.Username +" "+ model.Password);
                using (var client = new HttpClient())
                {
                    var serializedContent = JsonConvert.SerializeObject(new { Username = model.Username, Password = model.Password });
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(loginUri, stringContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError("AuthError", "Invalid username or password!");
                        return View(model);
                    }

                    var token = await response.Content.ReadAsStringAsync();
                    Response.Cookies.Append("accessToken", $"Bearer {token}", new CookieOptions {
                        Expires = DateTime.Now.AddHours(12)
                    });

                    client.DefaultRequestHeaders.Add(AUTHORIZATION_HEADER_NAME, $"Bearer {token}");

                    HttpResponseMessage loggedUserResponse = await client.PostAsync(usersUri,stringContent);

                    string jsonResponse = await loggedUserResponse.Content.ReadAsStringAsync();

                    UserVM loggedUser = JsonConvert.DeserializeObject<UserVM>(jsonResponse);


                    LoggedUser.Id = loggedUser.Id;
                    LoggedUser.Username = loggedUser.Username;
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Home");
        }


        // ----------------- Done
        public IActionResult Logout()
        {
            foreach (var cookie in HttpContext.Request.Cookies)
            {
                Response.Cookies.Delete(cookie.Key);
            }
            return RedirectToAction("Login", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
