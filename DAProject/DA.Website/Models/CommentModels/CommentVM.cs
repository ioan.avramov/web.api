﻿using DA.Website.Models.UserModel;
using DA.Website.Models.VideoModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.CommentModels
{
    public class CommentVM
    {
        public int Id { get; set; }

        [DisplayName("Says: ")]
        public string Text { get; set; }

        [DisplayName("Likes")]
        public int Likes { get; set; }

        [DisplayName("Disikes")]
        public double Dislikes { get; set; }
        public int OwnerId { get; set; }

        [DisplayName("Owner")]
        public UserVM Owner { get; set; }
        public int VideoId { get; set; }
        public VideoVM Video { get; set; }
    }
}
