﻿using DA.Website.Models.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.CommentModels
{
    public class CommentEditVM
    {
        [MaxLength(100)]
        [DisplayName("Text: ")]
        [Required(ErrorMessage = "Please enter something here!")]
        public string Text { get; set; }

        public int VideoId { get; set; }

        public int OwnerId { get; set; }


    }
}
