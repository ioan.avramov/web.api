﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models
{
    public class LogInModel
    {
        [DisplayName("Username: ")]
        [Required(ErrorMessage = "*Required field")]
        public string Username { get; set; }

        [DisplayName("Password: ")]
        [Required(ErrorMessage = "*Required field")]
        [PasswordPropertyText]
        public string Password { get; set; }

    }
}
