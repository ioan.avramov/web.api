﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.HomeModels
{
    public class RegisterVM
    {
        public int Id { get; set; }

        [MaxLength(30)]
        [DisplayName("Username: ")]
        [Required(ErrorMessage = "Please enter something here.")]
        public string Username { get; set; }

        [MaxLength(50)]
        [DisplayName("First Name: ")]
        [Required(ErrorMessage = "Please enter something here.")]
        public string First_Name { get; set; }

        [MaxLength(50)]
        [DisplayName("Last Name: ")]
        [Required(ErrorMessage = "Please enter something here.")]
        public string Last_Name { get; set; }

        [DisplayName("Age: ")]
        [Required(ErrorMessage = "Please enter something here.")]
        public double Age { get; set; }


        [Required(ErrorMessage = "Please enter something here.")]
        [DisplayName("Gender: ")]
        public bool Gender { get; set; }

        [MaxLength(30)]
        [Required(ErrorMessage = "Please enter something here.")]
        [PasswordPropertyText]
        [DisplayName("Password: ")]
        public string Password { get; set; }
    }
}
