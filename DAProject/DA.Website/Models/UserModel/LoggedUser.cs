﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.UserModel
{
    public static class LoggedUser
    {
        public static int Id { get; set; }
        public static string Username { get; set; }
    }
}
