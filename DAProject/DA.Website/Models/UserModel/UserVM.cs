﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.UserModel
{
    public class UserVM
    {
      
        public int Id { get; set; }

        [DisplayName("Username: ")]
        public string Username { get; set; }

        [DisplayName("First Name: ")]
        public string First_Name { get; set; }

        [DisplayName("Last Name: ")]
        public string Last_Name { get; set; }

        [DisplayName("Age: ")]
        public double Age { get; set; }

        [DisplayName("Gender: ")]
        public bool Gender { get; set; }
        
    }
}
