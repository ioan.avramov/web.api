﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.VideoModels
{
    public class VideoCreateVM
    {
        public int Id { get; set; }

        [MaxLength(30)]
        [Required(ErrorMessage ="Please enter something here")]
        public string Title { get; set; }

        [MaxLength(30)]
        [Required(ErrorMessage ="Please enter something here")]
        public string Link { get; set; }

        [DisplayName("Adult content")]
        public bool Over18 { get; set; }
    }
}
