﻿using DA.Website.Models.CommentModels;
using DA.Website.Models.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DA.Website.Models.VideoModels
{
    public class VideoVM
    {
     
        [DisplayName("Title")]
        public string Title { get; set; }


        [DisplayName("Video")]
        public string Link { get; set; }


        [DisplayName("Audiance")]
        public bool Over18 { get; set; }


        [DisplayName("Likes")]
        public int Likes { get; set; }


        [DisplayName("Owner")]
        public UserVM Owner { get; set; }

        public int Id { get; set; }

        public IEnumerable<CommentVM> Comments { get; set; }

    }
}
