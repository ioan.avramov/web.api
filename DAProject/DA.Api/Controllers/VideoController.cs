﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DA.Business.DTOs;
using DA.Business.Services;
using DA.DataModels.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DA.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VideoController : Controller
    {
        private readonly VideoService videoService;
        public VideoController()
        {
            this.videoService = new VideoService();
        }
        // GET: api/Video
        [HttpGet]
        public IEnumerable<VideoDto> GetAll([FromQuery] string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return videoService.GetAll().ToList();
            }
            return videoService.GetAll(title).ToList();
        }

        // GET: api/Video/title
        [HttpGet("{id}")]
        public VideoDto Get([FromRoute]int id)
        {
            return videoService.GetById(id);
        }

        // POST: api/Video
        [HttpPost]
        public IActionResult Create([FromBody] VideoDto videoDto)
        {
            var videos = videoService.GetAll();
            var video = videos.FirstOrDefault(v => v.Link == videoDto.Link);

            if (video == null && IsValid(videoDto))
            {
                videoService.Create(videoDto);
                return Ok();
            }
            return BadRequest();
        }

        // PUT: api/Video/5
        [HttpPut("{id}")]
        public IActionResult Update([FromRoute]int id, [FromBody] VideoDto videoDto)
        {
            videoDto.Id = id;
            var videos = videoService.GetAll();
            var video = videos.FirstOrDefault(v => v.Link == videoDto.Link);
            if (video == null && IsValid(videoDto))
            {
                videoService.Update(videoDto);
                return Ok();
            }
            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete([FromRoute]int id)
        {
            CommentService commentService = new CommentService();
           
            foreach (var comment in commentService.GetAll(id))
            {
                commentService.Delete(comment.Id);
            }
            videoService.Delete(id);
        }

        public bool IsValid(VideoDto video)
        {
            if (video.Title.Length <= 30 && video.Link.Length <= 50)
                return true;
            
            return false;
        }
    }
}
