﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using DA.Business.DTOs;
using DA.Business.Services;
using DA.DataModels.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService userService = new UserService();
        // GET: api/User
        [Authorize]
        [HttpGet]
        public IEnumerable<UserDto> Get([FromQuery] string username)
        {
            return userService.GetAll(username);
        }
        [HttpPost]
        public IActionResult Get([FromBody] UserDto user)
        {
            if (string.IsNullOrEmpty(user.First_Name) && string.IsNullOrEmpty(user.Last_Name))
            {
                UserDto curr = GetUserDto(user);
                return Ok(curr);
            }
            else
            {
                return Create(user);
            }
        }
        [Authorize]
        public UserDto GetUserDto(UserDto user)
        {
            var users = userService.GetAll();
            var foundUser = users.FirstOrDefault(u => u.Username == user.Username);

            if (foundUser != null)
            {
                return new UserDto
                {
                    Id = foundUser.Id,
                    Username = foundUser.Username,
                    Age = foundUser.Age,
                    Gender = foundUser.Gender,
                    First_Name = foundUser.First_Name,
                    Last_Name = foundUser.Last_Name
                };
            }
            return null;
        }
        
        public IActionResult Create(UserDto user)
        {
            var users = userService.GetAll();
            var currentUser = users.FirstOrDefault(u => u.Username == user.Username);

            if (currentUser == null && IsValid(user))
            {
                userService.Create(user);
                return Ok();
            }
            return BadRequest();
        }

        // PUT: api/User/5
        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UserDto user)
        {
            user.Id = id;
            var users = userService.GetAll();
            var currentUser = users.FirstOrDefault(u => u.Username == user.Username);
            if (currentUser == null && IsValid(user))
            {
                userService.Update(user);
                return Ok();
            }
            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [Authorize]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CommentService commentService = new CommentService();
            VideoService videoS = new VideoService();
            var videos = videoS.GetAll();

            foreach (var video in videos)
            {
                if (video.Owner.Id == id)
                {
                    foreach (var comment in video.Comments)
                    {
                        commentService.Delete(comment.Id);
                    }
                    videoS.Delete(video.Id);
                }
            }
            var user = userService.GetById(id);
            foreach (var comment in commentService.GetAll(user.Username))
            {
                commentService.Delete(comment.Id);
            }
            userService.Delete(user.Id);
        }
        public bool IsValid(UserDto user)
        {
            if (user.Username.Length <= 30 && user.First_Name.Length <= 50 && user.Last_Name.Length <= 50 && user.Password.Length <= 30)
                return true;

            return false;
        }
    }
}
