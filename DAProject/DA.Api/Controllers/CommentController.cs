﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using DA.Business.DTOs;
using DA.Business.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DA.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommentController : Controller
    {
        private readonly CommentService commentService;

        public CommentController()
        {
            commentService = new CommentService();
        }
        // GET: api/Comment
        [HttpGet]
        public IEnumerable<CommentsDto> GetAll(int videoId)
        {
            return commentService.GetAll(videoId);
        }

        // GET: api/Comment/userId
        //[HttpGet("{userId}")]
        //public IEnumerable<CommentsDto> Get([FromRoute]int userId)
        //{
        //    UserService userService = new UserService();
        //    UserDto userDto = userService.GetById(userId);
        //    return commentService.GetAll(userDto.Id);
        //}
        [HttpGet("{id}")]
        public CommentsDto GetOne([FromRoute]int id)
        {
            return commentService.GetById(id);
        }

        // POST: api/Comment
        [HttpPost]
        public void Create([FromBody] CommentsDto commentDto)
        {
            if (IsValid(commentDto))
                 commentService.Create(commentDto);
        }

        // PUT: api/Comment/5
        [HttpPut("{id}")]
        public void Update(int id, [FromBody] CommentsDto commentDto)
        {
            commentDto.Id = id;
            if (IsValid(commentDto))
                commentService.Update(commentDto);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            commentService.Delete(id);
        }

        public bool IsValid(CommentsDto comment)
        {
            if (comment.Text.Length <= 100)
                return true;

            return false;
        }
    }
}
