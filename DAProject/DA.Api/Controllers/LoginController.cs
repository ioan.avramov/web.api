﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DA.Business.DTOs;
using DA.Business.Services;
using DA.DataModels.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DA.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly IConfiguration config;

        public LoginController(IConfiguration config)
        {
            this.config = config;
        }

        //[HttpGet]
        //public IActionResult Get([FromBody] UserDto user)
        //{
        //    UserDto resultUser = AuthenticateUser(user);
        //    if (resultUser != null)
        //    {
        //        return Ok(resultUser);
        //    }
        //    return BadRequest();
        //}

        [HttpPost]
        public IActionResult Login([FromBody]UserDto user)
        {
            //Debug.WriteLine(user.Username + " " + user.Password);
            UserDto resultUser = AuthenticateUser(user);

            if (resultUser != null)
            {
                var token = GenerateJsonWebToken(resultUser);

                return Ok(token);
            }

            return Unauthorized();
        }

        private object GenerateJsonWebToken(UserDto user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:SecretKey"]));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new Claim[]
            {
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(
                issuer: config["Jwt:Issuer"],
                audience: config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(60),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserDto AuthenticateUser(UserDto user)
        {
            UserService userService = new UserService();
            var users = userService.GetAll();
            
            var foundUser = users.FirstOrDefault(u => u.Username == user.Username && u.Password == user.Password);

            if (foundUser != null)
            {
                return new UserDto
                {
                    Id = foundUser.Id,
                    Username = foundUser.Username,
                    Age = foundUser.Age,
                    Gender = foundUser.Gender,
                    First_Name = foundUser.First_Name,
                    Last_Name = foundUser.Last_Name
                };
            }
            return null;
        }
    }
}