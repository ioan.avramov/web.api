﻿using DA.Business.DTOs;
using DA.DataModels.Entities;
using DA.DataWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DA.Business.Services
{
    public class VideoService
    {
        public IEnumerable<VideoDto> GetAll(string title = null)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var videos = title != null
                    ? unitOfWork.VideoRepository.GetAll(d => d.Title == title)
                    : unitOfWork.VideoRepository.GetAll();
                CommentService cms = new CommentService();
                UserService us = new UserService();
                return videos.Select(video => new VideoDto
                {
                    Id = video.Id,
                    Title = video.Title,
                    Likes = video.Likes,
                    Link = video.Link,
                    DateAdded = video.DateAdded,
                    Over18 = video.Over18,
                    Owner = us.GetById(video.OwnerId),
                    Comments = cms.GetAll(video.Id)

                }).ToList();
            }
        }

        public VideoDto GetById(int id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var video = unitOfWork.VideoRepository.GetById(id);
                UserService us = new UserService();
                CommentService cs = new CommentService();
                return video == null ? null : new VideoDto
                {
                    Id = video.Id,
                    Title = video.Title,
                    Likes = video.Likes,
                    Link = video.Link,
                    DateAdded = video.DateAdded,
                    Over18 = video.Over18,
                    Owner = us.GetById(video.OwnerId),
                    Comments = cs.GetAll(video.Id)
                };
            }
        }

        public bool Create(VideoDto videoDto)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Random rand = new Random();
                var video = new Video()
                {
                    Title = videoDto.Title,
                    Likes = rand.Next(1),
                    Link = videoDto.Link,
                    DateAdded = DateTime.Now,
                    Over18 = videoDto.Over18,
                    OwnerId = videoDto.OwnerId
                };

                unitOfWork.VideoRepository.Create(video);

                return unitOfWork.Save();
            }
        }

        public bool Update(VideoDto videoDto)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var result = unitOfWork.VideoRepository.GetById(videoDto.Id);

                if (result == null)
                {
                    return false;
                }

                result.Title = videoDto.Title;
                result.Link = videoDto.Link;
                result.Over18 = videoDto.Over18;

                unitOfWork.VideoRepository.Update(result);

                return unitOfWork.Save();
            }
        }

        public bool Delete(int id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Video result = unitOfWork.VideoRepository.GetById(id);

                if (result == null)
                {
                    return false;
                }

                unitOfWork.VideoRepository.Delete(result);

                return unitOfWork.Save();
            }
        }
    }
}
