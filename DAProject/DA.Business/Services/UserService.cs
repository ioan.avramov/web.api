﻿using DA.Business.DTOs;
using DA.DataModels.Entities;
using DA.DataWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DA.Business.Services
{
    public class UserService
    {
        public IEnumerable<UserDto> GetAll(string username = null)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var users = username != null
                    ? unitOfWork.UserRepository.GetAll(d => d.Username == username)
                    : unitOfWork.UserRepository.GetAll();

                return users.Select(user => new UserDto
                {
                    Id = user.Id,
                    First_Name = user.First_Name,
                    Last_Name = user.Last_Name,
                    Gender = user.Gender,
                    Age = user.Age,
                    DateAdded = user.DateAdded,
                    Username = user.Username,
                    Password = user.Password
                });
            }
        }

        public UserDto GetById(int id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var user = unitOfWork.UserRepository.GetById(id);

                return user == null ? null : new UserDto
                {
                    Id = user.Id,
                    Username = user.Username,
                    First_Name = user.First_Name,
                    Last_Name = user.Last_Name,
                    Age = user.Age,
                    Gender = user.Gender,
                    DateAdded = user.DateAdded
                    
                };
            }
        }

        public bool Create(UserDto userDto)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var user = new User()
                {
                    First_Name = userDto.First_Name,
                    Last_Name = userDto.Last_Name,
                    DateAdded = DateTime.Now,
                    Password = userDto.Password,
                    Gender = userDto.Gender,
                    Age = userDto.Age,
                    Username = userDto.Username
                };

                unitOfWork.UserRepository.Create(user);

                return unitOfWork.Save();
            }
        }

        public bool Update(UserDto userDto)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var result = unitOfWork.UserRepository.GetById(userDto.Id);

                if (result == null)
                {
                    return false;
                }

                result.First_Name = userDto.First_Name;
                result.Last_Name = userDto.Last_Name;
                result.Username = userDto.Username;
                result.Password = userDto.Password;
                result.Age = userDto.Age;
                result.Gender = userDto.Gender;

                unitOfWork.UserRepository.Update(result);

                return unitOfWork.Save();
            }
        }

        public bool Delete(int id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                User result = unitOfWork.UserRepository.GetById(id);

                if (result == null)
                {
                    return false;
                }

                unitOfWork.UserRepository.Delete(result);

                return unitOfWork.Save();
            }
        }
    }
}
