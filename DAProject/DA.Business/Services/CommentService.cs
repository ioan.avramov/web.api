﻿using DA.Business.DTOs;
using DA.DataModels.Entities;
using DA.DataWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DA.Business.Services
{
    public class CommentService
    {
        private readonly UserService us = new UserService();
        private readonly VideoService vs = new VideoService();
        private Random rand = new Random();

        public IEnumerable<CommentsDto> GetAll(int id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var comments = unitOfWork.CommentRepository.GetAll(d => d.VideoId == id);
               
                return comments.Select(comment => new CommentsDto
                {
                    Id = comment.Id,
                    Text = comment.Text,
                    Likes = comment.Likes,
                    Dislikes = comment.Dislikes,
                    DateAdded = comment.DateAdded,
                    Owner = us.GetById(comment.OwnerId),
                    VideoId = comment.VideoId

                }).ToList();
            }
        }

        public IEnumerable<CommentsDto> GetAll(string ownerUsername)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var comments = unitOfWork.CommentRepository.GetAll(d => d.Owner.Username == ownerUsername);

                return comments.Select(comment => new CommentsDto
                {
                    Id = comment.Id,
                    Likes = comment.Likes,
                    Dislikes = comment.Dislikes,
                    DateAdded = comment.DateAdded,
                    Owner = us.GetById(comment.OwnerId)

                }).ToList();
            }
        }
        public CommentsDto GetById(int Id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var comment = unitOfWork.CommentRepository.GetById(Id);
                return comment == null ? null : new CommentsDto
                {
                    Text = comment.Text,
                    Id = comment.Id,
                    Likes = comment.Likes,
                    Dislikes = comment.Dislikes,
                    DateAdded = comment.DateAdded,
                    Owner = us.GetById(comment.OwnerId),
                    VideoId = comment.VideoId
                };
            }
        }

        public bool Create(CommentsDto commentsDto)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            { 
                var comment = new Comment()
                {
                    Text = commentsDto.Text,
                    Likes = rand.Next(1,10),
                    Dislikes = rand.Next(1,10),
                    DateAdded = DateTime.Now,
                    OwnerId = commentsDto.OwnerId,
                    VideoId = commentsDto.VideoId,
                };

                unitOfWork.CommentRepository.Create(comment);

                return unitOfWork.Save();
            }
        }

        public bool Update(CommentsDto commentsDto)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var result = unitOfWork.CommentRepository.GetById(commentsDto.Id);

                if (result == null)
                {
                    return false;
                }

                result.Text = commentsDto.Text;

                unitOfWork.CommentRepository.Update(result);

                return unitOfWork.Save();
            }
        }

        public bool Delete(int id)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Comment result = unitOfWork.CommentRepository.GetById(id);

                if (result == null)
                {
                    return false;
                }

                unitOfWork.CommentRepository.Delete(result);

                return unitOfWork.Save();
            }
        }
    }
}
