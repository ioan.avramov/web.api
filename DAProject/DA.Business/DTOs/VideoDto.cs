﻿using DA.DataModels.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.DTOs
{
    public class VideoDto : BaseDto
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public bool Over18 { get; set; }
        public int Likes { get; set; }
        public UserDto Owner { get; set; }
        public int OwnerId { get; set; }
        public IEnumerable<CommentsDto> Comments { get; set; }
    }
}
