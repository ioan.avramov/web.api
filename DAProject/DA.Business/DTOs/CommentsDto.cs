﻿using DA.DataModels.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.DTOs
{
    public class CommentsDto : BaseDto
    {
        public string Text { get; set; }
        public int Likes { get; set; }
        public double Dislikes { get; set; }
        public int OwnerId { get; set; }
        public UserDto Owner { get; set; }
        public int VideoId { get; set; }
        public VideoDto Video_ { get; set; }
    }
}
