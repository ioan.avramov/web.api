﻿using System;

namespace DA.Business.DTOs
{
    public class BaseDto
    {
        public int Id { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
