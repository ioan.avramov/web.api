﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.DTOs
{
    public class UserDto : BaseDto
    {
        public string Username { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public double Age { get; set; }
        public bool Gender { get; set; }
        public string Password { get; set; }
    }
}
